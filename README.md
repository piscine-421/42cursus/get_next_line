# get_next_line

Function created for the 42 project get_next_line. Passes moulinette with a final score of 125/100.

## Description

This function reads the next line of a specified file descriptor. Each function call will advance by one line. Multiple file descriptors can be used concurrently in the bonus version, but not the regular version. The largest usable file descriptor usable in the bonus version is defined by the MAX_FD macro (defaulting to 100). The size of the buffer used for both versions is defined by the BUFFER_SIZE macro (defaulting to 10).

## Building

Regular version:

	cc -Wall -Werror -Wextra -c get_next_line.c get_next_line_utils.c
	ar rcs getnextline.a get_next_line.o get_next_line_utils.o
	rm get_next_line.o get_next_line_utils.o

Bonus version:

	cc -Wall -Werror -Wextra -c get_next_line_bonus.c get_next_line_utils_bonus.c
	ar rcs getnextline.a get_next_line_bonus.o get_next_line_utils_bonus.o
	rm get_next_line_bonus.o get_next_line_utils_bonus.o

Test program:

	cc -Wall -Werror -Wextra -c get_next_line_bonus.c get_next_line_utils_bonus.c
	ar rcs getnextline.a get_next_line_bonus.o get_next_line_utils_bonus.o
	cc -Wall -Werror -Wextra getnextline.a main.c
	rm get_next_line_bonus.o get_next_line_utils_bonus.o getnextline.a

## Test Program

Meant for use with the bonus version of the program exclusively. Uses get_next_line to print the contents of text files. Doesn't check if the results are correct. Arguments can be used to pick a file and the amount of lines to be printed. If these arguments are absent, the files provided in this repository will be used.
