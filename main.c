/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/23 18:06:33 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ctype.h>
#include <fcntl.h>
#include "get_next_line.h"
#include <stdio.h>
#include <string.h>

static int	getsize(long n)
{
	int	s;

	s = 1;
	if (!n)
		s++;
	if (n < 0)
		s++;
	while (n && s++)
		n /= 10;
	return (s);
}

static char	*ft_itoa(int n)
{
	char		*returned;
	int			i[2];
	long int	n2;

	i[0] = 0;
	i[1] = 1000000000;
	n2 = n;
	returned = malloc(getsize(n) * sizeof(char));
	if (!returned)
		return (returned);
	if (n2 < 0)
	{
		returned[i[0]++] = '-';
		n2 *= -1;
	}
	while (i[1])
	{
		if (n2 / i[1])
			returned[i[0]++] = n2 / i[1] % 10 + '0';
		i[1] /= 10;
	}
	if (!n)
		returned[i[0]++] = '0';
	returned[i[0]++] = '\0';
	return (returned);
}

static char	*escape(const char *str)
{
	char	*cn;
	int		i;
	int		i2;
	int		n;
	char	*str2;

	i = 0;
	i2 = 0;
	n = 0;
	if (!str)
		return ("NULL");
	while (str[i])
	{
		if (str[i] == '\a' || str[i] == '\b' || str[i] == '\v' || str[i] == '\f' || str[i] == '\r' || str[i] == '\e')
			n++;
		else if (!isprint(str[i]) && str[i] != '\t' && str[i] != '\n')
		{
			n++;
			if ((unsigned char)str[i] > 9)
				n++;
			if ((unsigned char)str[i] > 99)
				n++;
		}
		i++;
	}
	str2 = malloc((strlen(str) + n + 1) * sizeof(char));
	i = 0;
	while (str[i])
	{
		if (str[i] == '\a')
		{
			str2[i2++] = '\\';
			str2[i2++] = 'a';
		}
		else if (str[i] == '\b')
		{
			str2[i2++] = '\\';
			str2[i2++] = 'b';
		}
		else if (str[i] == '\n')
		{
			(void)0;
		}
		else if (str[i] == '\v')
		{
			str2[i2++] = '\\';
			str2[i2++] = 'v';
		}
		else if (str[i] == '\f')
		{
			str2[i2++] = '\\';
			str2[i2++] = 'f';
		}
		else if (str[i] == '\r')
		{
			str2[i2++] = '\\';
			str2[i2++] = 'r';
		}
		else if (str[i] == '\e')
		{
			str2[i2++] = '\\';
			str2[i2++] = 'e';
		}
		else if (!isprint(str[i]) && str[i] != '\t')
		{
			str2[i2++] = '\\';
			cn = ft_itoa((unsigned char)str[i]);
			str2[i2++] = cn[0];
			if ((unsigned char)str[i] > 9)
				str2[i2++] = cn[1];
			if ((unsigned char)str[i] > 99)
				str2[i2++] = cn[2];
			free(cn);
		}
		else
			str2[i2++] = str[i];
		i++;
	}
	str2[i2++] = '\0';
	return (str2);
}

static char	*ft_strdup2(char *s1)
{
	char	*s2;
	int		i;

	i = 0;
	if (!s1)
		return (ft_strdup2("(null)"));
	while (s1 && s1[i])
		i++;
	s2 = malloc((i + 1) * sizeof(char));
	if (!s2)
		return (s2);
	strlcpy(s2, s1, i + 1);
	return (s2);
}

static int	i_d(char *s)
{
	int	i;

	i = 0;
	if (s && s[i] == '-')
		i++;
	while (s && s[i])
	{
		if (!isdigit(s[i]))
			return (0);
		i++;
	}
	return (1);
}

static int	i_s(char *s)
{
	return (s && !strcmp(s, "-v"));
}

static int	test_get_next_line(char *filename, int iter)
{
	int			file[100];
	static int	i = 0;
	int			i2;
	char		*str;

	i2 = 1;
	if (!filename && !iter)
	{
		while (i)
		{
			close(file[i]);
			i--;
		}
		printf("\n");
		return (0);
	}
	file[i] = open(filename, 0);
	if (file[i] == -1)
		return (-1);
	printf("\n%s:\n", filename);
	while (iter--)
	{
		str = ft_strdup2(get_next_line(file[i]));
		if (!strchr(str, '\n'))
			printf("%-4.3d%s$\n", i2++, escape(str));
		else
			printf("%-4.3d%s\n", i2++, escape(str));
		if (str && iter)
			free(str);
	}
	str = ft_strdup2(get_next_line(file[i]));
	if (str && iter)
		free(str);
	return (0);
}

int	main(int argc, char **argv)
{
	(void)argc;
	if (i_s(argv[1]) && i_d(argv[2]))
	{
		if (test_get_next_line(argv[1], atoi(argv[2])) == -1)
			printf("\nFile \"%s\" not found.\n", argv[1]);
		test_get_next_line(0, 0);
		return (0);
	}
	test_get_next_line("test/test.txt", 16);
	test_get_next_line("test/single.txt", 1);
	test_get_next_line("test/empty.txt", 2);
	test_get_next_line("test/newlines.txt", 6);
	test_get_next_line("test/singlechar.txt", 1);
	test_get_next_line("test/singlenewline.txt", 1);
	test_get_next_line("test/1char.txt", 2);
	test_get_next_line("test/one_line_no_nl.txt", 2);
	test_get_next_line("test/only_nl.txt", 2);
	test_get_next_line("get_next_line.c", 91);
	test_get_next_line(0, 0);
}
